$(document).ready(function(){
    var sliders = $('[data-slider-src]');
    $.each(sliders, function(i, e){
        
        var settings = {
        	show_for_screen:570, 
        	auto_animate:false, 
        	bullet_navigation:true, 
        	side_navigation:true,
        	speed_of_transition:3000
        };//720
        
        var nSlider = new dbSlider($(e), settings);
    });
});