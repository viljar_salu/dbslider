var dbSlider = function(el, settings) {
    var slider, 
        screenVp, 
        slides, 
        self = this, 
        uniqID, 
        sliderWrapper,
        originSlider,
        activeSlide = 0,
        timer,
        prevNav,
        nextNav,
        settings;

    this.init = function() {
    	this.settings		= settings;

    	if( $(window).width() > this.settings.show_for_screen ) {
	    	return true;
	    }
	    
    	this.uniqID         = 'db_slider-' + new Date().getTime();
        this.screenVp       = $(window).width(); 
        this.originSlider   = el;
        this.altSlides		= (this.originSlider.data('sliderSrc').length ? this.originSlider.data('sliderSrc').split(',') : 0);
        this.slides         = (this.altSlides ? this.altSlides : this.originSlider.children());
        this.sliderWrapper  = $('<div class="dbSlider-wrapper"></div>');
        this.slider         = $('<ul class="dbSlider"></ul>');
        this.prevNav		= $('<div class="dbSlider__prevNav"></div>');
        this.nextNav		= $('<div class="dbSlider__nextNav"></div>');

        this.originSlider.parent().append(this.sliderWrapper);
        this.slider.width( this.slides.length * this.screenVp + 14);

        this.sliderWrapper.append(this.slider);
        
        this.showHideSlider();
        this.create();

        if(this.settings.side_navigation && this.slides.length > 1) {
            this.createSideNav();
        }
        if(this.settings.bullet_navigation && this.slides.length > 1) {
            this.createBulletNav();
        }
        if(this.settings.auto_animate && this.slides.length > 1) {
        	/*this.startTimer();

        	this.sliderWrapper.on('mouseleave', function() {
        		if( self.sliderWrapper.hasClass('paused') ) {
        			console.log('start timer');
        			self.startTimer();
        		}
        	});*/
            self.sliderWrapper.removeClass('paused');
        }

    }

    this.create = function() {
    	var _content;
        $.each(self.slides, function(i, e) {
            var slide_tpl = $('<li class="dbSlider__item '+(i==0 ? "active" : "")+'"></li>');
            slide_tpl.width(self.screenVp);
            slide_tpl.addClass('dbSlide-'+i);

            if(self.altSlides) {
            	_content = '<img src="'+e+'"/>';
            } else {
            	_content = $(e).clone();
            }
            
            slide_tpl.append( _content );
            self.slider.append(slide_tpl);
        });
    }

    this.showHideSlider = function() {
    	if( $(window).width() > this.settings.show_for_screen ) {
	    	this.sliderWrapper.addClass('dbSlider-wrapper__hided');
	    	this.originSlider.show();
	    	this.killTimer();
	    } else {
	    	this.sliderWrapper.removeClass('dbSlider-wrapper__hided');
	    	this.originSlider.hide();
	    	
	    	if(this.settings.auto_animate) {
	    		this.startTimer();
	    	}
	    }
    }

    this.update = function() {
        this.screenVp = $(window).width(); 

        this.slider.width( this.slides.length * this.screenVp + 14);

        $.each(this.slider.children(), function(i, e) {
            $(e).width(self.screenVp);
        });

        this.goToSlide(0);
        this.showHideSlider();
    }

    this.getPrevSlideIndex = function() {
    	var _index = this.slider.find('.active').index();
    	_index = (_index > 0 ? _index - 1 : (self.slides.length - 1));
    	return _index;
    }

    this.getNextSlideIndex = function() {
    	var _index = this.slider.find('.active').index();
    	_index = (_index < (self.slides.length - 1) ? _index + 1 : 0);
    	return _index;	
    }

    this.createSideNav = function() {
    	
    	this.prevNav.on('click', function(e){
    		self.killTimer();
    		self.goToSlide( self.getPrevSlideIndex() );
    	});

    	this.nextNav.on('click', function(e){
    		self.killTimer();
    		self.goToSlide( self.getNextSlideIndex() );
    	});

    	this.sliderWrapper.append(this.prevNav);
        this.sliderWrapper.append(this.nextNav);
    }

    this.createBulletNav = function() {
        var bulletNav_tpl = $('<ul class="dbSlider__bullet-nav"></ul>');
        
        $.each(self.slides, function(i, e) {
            var bullet_tpl = $('<li class="dbSlider__bullet '+(i==0 ? "active" : "")+'"></li>');
            bullet_tpl.addClass('dbSlide-'+i);
            bullet_tpl.on('click', function(){
                self.goToSlide(i);
                self.killTimer();
            });
            bulletNav_tpl.append(bullet_tpl);
        });

        this.sliderWrapper.append(bulletNav_tpl);
    }

    this.goToSlide = function(index) {
        var slideIndex = (index * this.screenVp) * -1;
        this.activeSlide = index;
        this.slider.css('transform','translateX('+slideIndex+'px)');
        this.selectActiveSlide(index);
    }

    this.selectActiveSlide = function(index) {
    	this.sliderWrapper.find('[class*="dbSlide-"]').removeClass('active');
    	this.sliderWrapper.find('.dbSlide-'+index).addClass('active');
    }

    // TODO:
    this.startTimer = function() {
    	
    	//this.sliderWrapper.removeClass('paused');

        this.timer = setInterval(function(){
            /*console.log('next slide:', self.getNextSlideIndex());
            self.goToSlide( self.getNextSlideIndex() );*/

            if(!self.sliderWrapper.hasClass('paused')) {
                self.goToSlide( self.getNextSlideIndex() );
            }

        }, this.settings.speed_of_transition);
    }
    this.killTimer = function() {
    	this.sliderWrapper.addClass('paused');
    	//clearInterval(this.timer);
    }


    // initialize app
    this.init();

    $(window).on('resize', _.debounce(function(){
    	if( self.slider) {
	        console.log('allready initalasdfjs');
	        self.update();
	        return true;
	    } else {
	    	self.init();
	    }
    }, 500));
}